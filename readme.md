# <b class="fa-solid fa-wand-sparkles"></b> CréaCarte <b class="fa-solid fa-id-card"></b> <br> Un Éditeur de Carte Interactif 

## Table des matières

1. [Introduction](#introduction)
2. [Fonctionnalité](#fonctionnalité) (avec une note d'aide à l'[impression <b class="fa-solid fa-print"></b>](#impression))
3. [Prérequis/Installation](#install)
4. [Utilisation](#utilisation)
5. [Guide de personnification](#guide)
	- [Modifier le texte](#texte)
	- [Modifier les images](#image)
	- [Modifier les couleurs des cartes](#couleur)
	- [Ajouter de nouvelles cartes](#ajout)
6. [Un exemple : Périmètre et aire de figures usuelles](#exemple)
6. [Contribution et licence](#contribution)

<p style="text-align:right;">Documentation au format pdf : [<b class="fa-regular fa-file-pdf"></i></b>](readme.pdf)</p>

![](capture1.png)


<p id="introduction"></p>
## Introduction
CréaCarte est une application web destinée à concevoir et éditer des cartes interactives personnalisées à imprimer. Elle est idéale pour les enseignants, les créateurs de contenu éducatif, ou toute personne intéressée par la création de supports pédagogiques visuels.

<p id="fonctionnalité"></p>
## Fonctionnalités
- **Éditeur en Temps Réel :** Modifiez et visualisez instantanément les changements apportés à vos cartes.

- **Sauvegarde et Exportation :** Sauvegardez vos cartes en format HTML ou imprimez-les directement en PDF.

- **Personnalisation Avancée :** Utilisez diverses options de style pour personnaliser l'apparence de vos cartes.

- **Insertin d'icones :** Vous pouvez insérer des icones grâce à la [version 6 de FontAwesome](https://fontawesome.com/v6/download).
Par exemple : `<i class="fa-regular fa-hand-peace"></i>` produira <b class="fa-regular fa-hand-peace"></b>.
Retrouvez toutes les icones disponibles sur [cette page](https://fontawesome.com/v6/search?o=r&m=free).

- **Écrire des maths :** Vous pouvez écrire des équations mathématiques en $\LaTeX$ grâce à [Mathjax](https://www.mathjax.org/).
Par exemple : `$Aire_{triangle}=\dfrac{base\times{}hauteur}{2}$` donnera : $Aire_{triangle}=\dfrac{base\times{}hauteur}{2}$.
Plus d'information sur les codes sur la page [Zeste de savoir - Des Mathématiques](https://zestedesavoir.com/tutoriels/826/introduction-a-latex/1322_completer-vos-documents/5376_des-mathematiques/).

- <strong id="impression">Impression au format PDF :</strong> Imprimez au format PDF. Pensez à activer l'impression des arrières-plans (cf image ci-dessous).

![Copie d'écran de l'activation des arrières pland sur Firefox et Brave](impressionAP.png)

<p id="install"></p>
## Prérequis/Installation

### Prérequis 

- Un navigateur web moderne (Chrome, Firefox, Safari).
- Accès à Internet pour le chargement des ressources externes (Codemirror, FontAwesome et Mathjax).

### Installation
- Aucune installation n'est nécessaire. L'application fonctionne directement dans votre navigateur web.
- Pour plus de simplicité, vous pouvez télécharger et décompresser [cette archive <b class="fa-solid fa-box-archive"></b>](creacarte.zip) et ensuite ouvrir et travailler sur la page `index.html`. Vous aurez toujours besoin d'un accès à internet.

<p id="utilisation"></p>
## Utilisation
1. **Lancement de l'application :** Ouvrez le fichier `index.html` dans votre navigateur.
2. **Édition de carte :** Entrez le contenu HTML directement dans l'éditeur [Codemirror](https://codemirror.net/) à gauche de votre écran.
3. **Visualisation :** Observez le rendu dans la fenêtre de prévisualisation à droite.
4. **Sauvegarde :** Entrez un nom de fichier et cliquez sur "Enregistrer au format HTML" pour télécharger votre travail.
5. **Impression :** Cliquez sur "Imprimer en PDF" pour générer une version imprimable de votre carte. _Pensez à activer l’[impression des arrières-plans](#impression)._
6. **Reprendre son travail :** Copier-coller le code enregistré dans la fenêtre de code à gauche.


<p id="guide"></p>
## Guide de Personnalisation

<p id="texte"></p>
### Modification des Textes
Pour modifier les textes sur vos cartes :
1. Localisez le bloc de texte dans l'éditeur HTML à gauche de l'écran.
2. Modifiez directement le texte entre les balises appropriées (par exemple, `<div class="card-title">Votre Titre Ici</div>`).
3. Les modifications seront immédiatement visibles dans la fenêtre de prévisualisation à droite.

<p id="image"></p>
### Modification des Images
Pour changer les images :
1. Vous pouvez référencer une image en ligne via son URL ou utiliser une image stockée localement sur votre ordinateur.
2. Pour utiliser une image locale, enregistrez d'abord votre page HTML ([en téléchargeant et extrayant cette archive <i class="fa-solid fa-box-archive"></i>](creacarte.zip)) et travaillez en local en ouvrant la page `index.html`.
3. Ensuite, placez ensuite vos images (avec de bonnes dimensions) dans le même dossier que votre fichier HTML ou dans un sous-dossier.
4. Dans l'éditeur HTML, modifiez le chemin dans l'attribut `src` de l'élément `<img>` ou `background-image` de la CSS interne pour pointer vers votre fichier image (par exemple, `src="images/mon-image.jpg"` ou `background-image: url('images/mon-image.jpg');`).

<p id="couleur"></p>
### Modification des Couleurs des Cartes

1. Sélectionnez la `div` de la carte que vous souhaitez modifier.
2. Changez la classe de la carte pour correspondre à la couleur désirée. Voici les classes disponibles :
   - `card-blue` pour le bleu
   - `card-yellow` pour le jaune
   - `card-green` pour le vert
   - `card-purple` pour le violet
   - `card-orange` pour l'orange
   - `card-red` pour le rouge
   - le gris est la couleur par défaut

Par exemple, pour changer la couleur d'une carte en vert, remplacez la classe dans la `div` de cette manière :
```html
<div class="card card-green">
    <!-- Contenu de la carte ici -->
</div>
```

<p id="ajout"></p>
#### Ajout de Nouvelles Cartes

Pour ajouter de nouvelles cartes, copiez et collez le code HTML d'une carte existante, puis modifiez son contenu selon vos besoins. Veillez à ajuster la classe de couleur si nécessaire. Voici un exemple de code pour ajouter une nouvelle carte :

```html
<div class="card card-blue">
    <!-- Contenu de la nouvelle carte ici -->
</div>
```

<p id="exemple"></p>
## Un exemple : Périmètre et aire de figures usuelles

### Le code

Le code html est consultable [ici <b class="fa-solid fa-file-code"></b>](https://forge.apps.education.fr/lmdbt/creacarte/-/blob/main/exemple_p%C3%A9rim%C3%A8tre_et_aire.html?ref_type=heads).

### Aperçu du résultat

#### À l'impression

**En image**
![Copie d'écran](https://static.piaille.fr/media_attachments/files/112/399/752/630/187/281/original/5afcb0431acab2e3.jpg)

**En pdf**
- [Version téléchargeable du document <b class="fa-regular fa-file-pdf"></b>](https://forge.apps.education.fr/lmdbt/creacarte/-/blob/main/p%C3%A9rim%C3%A8tre-et-aire-pdf.pdf?ref_type=heads)

#### Après découpage et collage

![Photogaphie des cartes découpées, collées - recto verso](https://static.piaille.fr/media_attachments/files/112/399/752/857/451/425/original/5167a1440a479670.jpg)
![Photographie de l'utilisation des cartes par les élèves](https://static.piaille.fr/media_attachments/files/112/400/527/788/843/982/original/2f67e0162df1d053.jpg)


<p id="contribution"></p>
## Contribution et licence

### Contribution

Les contributions à ce projet sont les bienvenues ! Veuillez soumettre vos bugs, suggestions et demandes de fonctionnalités via le système de tickets de [La Forge sur ce projet](https://forge.apps.education.fr/lmdbt/creacarte/-/issues).

### Licence
CréaCarte est distribué sous la licence [<b class="fa-brands fa-creative-commons"></b><b class="fa-brands fa-creative-commons-by"></b> 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr). 
Vous êtes libre de l'utiliser, le modifier, et le distribuer en créditant son auteur.